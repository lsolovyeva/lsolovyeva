# Hi, I'm Ludmila 👋🏻

```
I'm a software engineer.
- Need a contributor to your project (review, impovements)?
- Want to talk about hobbies/lifestyle (sport, music, language)?
Reach out to me in tg: @lsolovyeva
```

## Tech stack

#### 📘 Programming languages:
- Java 8-17    ★★★★★
- Python       ★★★☆☆
- C/C++        ★★★☆☆

#### 📕 Frameworks and tools:
- Spring (Boot, Data JPA, Web, MVC, Config, Security, Cache, Test)
- Hibernate
- Tests: JUnit, Mockito, TestNG
- Monitoring: Sumologic, DataDog, OverOps

#### 📙 DBs:
- MySQL, 
- PostgreSQL, 
- Snowflake, H2

#### 📔 Cloud:
- AWS (SQS, SNS, S3)

#### 📗 VCS:
- Git
- SVN

#### 📓 Others:
- Liquibase
- SonarQube, Snyk
- REST API, Microservices, Docker, Maven, Apache Kafka
- GitHub, GitLab, Bitbucket 
- Confluence, Jira, TestRail
- JSON, XML
